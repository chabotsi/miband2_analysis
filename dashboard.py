#!/usr/bin/env python3
# coding: utf-8

from datetime import timedelta
import warnings

from numpy import poly1d, polyfit
from scipy.signal import argrelmin
from astral import Astral, Location
from matplotlib.patches import Rectangle
from matplotlib.collections import LineCollection
from matplotlib.colors import Normalize
from matplotlib import colorbar

import pylab as p
import pandas as pd

from model import (loaddata, get_shifts, get_slices_of_consecutive_values,
                   ActivityType, filtered_serie)

warnings.simplefilter('ignore', p.RankWarning)
FILTERING_FREQ=2e-4
CMAP = 'rainbow'

class PiecewiseNorm(Normalize):
    def __init__(self, levels, clip=False):
        # the input levels
        self._levels = p.sort(levels)
        # corresponding normalized values between 0 and 1
        self._normed = p.linspace(0, 1, len(levels))
        Normalize.__init__(self, None, None, clip)

    def __call__(self, value, clip=None):
        # linearly interpolate to get the normalized value
        return p.ma.masked_array(p.interp(value, self._levels, self._normed))

def get_colored_line(x, y, values, cmap, norm):
    points = p.array([x, y]).T.reshape(-1, 1, 2)
    segments = p.concatenate([points[:-1], points[1:]], axis=1)

    lc = LineCollection(segments, cmap=cmap, norm=norm)
    lc.set_array(values)
    return lc

def mean_hr_during_sleep(data):
    """ https://blog.ouraring.com/blog/heart-rate-while-sleeping/ """
    filtered_hr = filtered_serie(data.HEART_RATE, FILTERING_FREQ)
    filtered_mvt = filtered_serie(data.RAW_INTENSITY, FILTERING_FREQ)
    min_duration = 3*60 # min_duration below which no regression in made

    for sleep in get_shifts(data, ActivityType.SLEEPING):
        duration = len(sleep)
        time = sleep.DATETIME.apply(lambda x:x.timestamp())

        fall_asleep_datetime = sleep.iloc[0].DATETIME
        wakeup_datetime = sleep.iloc[-1].DATETIME
        fall_asleep_date = (fall_asleep_datetime - timedelta(hours=12)).date()
        weekday = fall_asleep_date.isoweekday()-1

        if duration > min_duration:
            polysleep = poly1d(polyfit(time, sleep.HEART_RATE, 4))
            p.plot(sleep.DATETIME, polysleep(time), c='forestgreen', ls='--', lw=1,
                   alpha=1)

        x = p.date2num(fall_asleep_datetime + timedelta(minutes=duration/2))
        y = filtered_hr.min()*0.95
        p.text(x, y, '{:01d} h {:01d}'.format(duration//60, duration%60),
               horizontalalignment='center', verticalalignment='center')

        try:
            minima = argrelmin(p.array(filtered_hr[sleep.index]))[0]
            avgduration = sum(p.diff(minima))/(len(minima) - 1)
            shortest = p.diff(minima).min()
            longest = p.diff(minima).max()
            template = 'On {}, you had {} cycles, avg duration = {:0.0f} min'
            print(template.format(fall_asleep_date, len(minima),
                                  avgduration), end=' ')
            print('({:0.0f} / {:0.0f})'.format(shortest, longest))
        except (ValueError, ZeroDivisionError):
            pass

        # add a bullet whose color informs you on how you move during the
        # night. blue => no movement < cyan < green < yellow < red => High movement.
        norm = Normalize(vmin=3, vmax=7)
        m = p.cm.ScalarMappable(norm=norm, cmap=CMAP)
        c = m.to_rgba(sleep.iloc[:-15].RAW_INTENSITY.mean()) # remove last 15 min
        p.plot(x, filtered_hr.max()*1.05, 'o', color=c)

        # plot the HR during the sleep whose color show you when you moved.
        x = [p.date2num(n) for n in sleep.index]
        y = filtered_hr[sleep.index]
        values = filtered_mvt[sleep.index]
        lc = get_colored_line(x, y, values, CMAP, norm)
        lc.set_linewidth(1.5)
        p.gca().add_collection(lc)

def mean_hr(data, max_heart_rate=190):
    filtered_hr = filtered_serie(data.HEART_RATE, FILTERING_FREQ)

    ax = p.gca()
    levels = p.array([0, 0.6, 0.7, 0.8, 0.9, 1])*max_heart_rate
    norm = PiecewiseNorm(levels)

    x = [p.date2num(n) for n in filtered_hr.index]
    y = filtered_hr
    lc = get_colored_line(x, y, y, CMAP, norm)
    ax.add_collection(lc)
    ax.set_ylim(0.9*filtered_hr.min(), 1.1*filtered_hr.max())


def nights(data, city):
    dates = set(data.DATETIME.apply(lambda x:x.date()))
    ax = p.gca()
    for date in dates:
        night = [p.date2num(n) for n in city.night(date)]
        day = [p.date2num(n) for n in city.daylight(date)]
        night_rect = Rectangle((night[0], 0), night[1] - night[0], 200,
                               color='grey', alpha=0.05)
        day_rect = Rectangle((day[0], 0), day[1] - day[0], 200,
                               color='C0', alpha=0.05)

        ax.add_patch(night_rect)
        ax.add_patch(day_rect)


if __name__ == '__main__':
    data = loaddata('miband.db', min_date='5 days ago', cached=False)
    city = Location(('Nice', 'France', 43.7, 7.25, 'Europe/Paris', 0))

    mean_hr(data, max_heart_rate=190)
    mean_hr_during_sleep(data)
    nights(data, city)

    norm = Normalize(vmin=0, vmax=1)
    sm = p.cm.ScalarMappable(cmap=CMAP, norm=Normalize(vmin=0, vmax=1))
    sm._A = []
    cbar = p.colorbar(sm, ticks=[0, 0.5, 1]) #, orientation='horizontal')
    cbar.ax.set_yticklabels(['low', 'med', 'high'])

    p.xlim(data.index[0], data.index[-1])
    p.gcf().autofmt_xdate()
    p.grid()
    p.tight_layout()
    p.show()
