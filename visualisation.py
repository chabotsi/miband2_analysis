#!/usr/bin/env python3
# coding: utf-8

from enum import Enum
from datetime import datetime, timedelta

import pylab as p
import pandas as pd

from model import loaddata, get_shifts, ActivityType, filtered_serie
from sleep import get_sleep_df, get_sleep_score

def _plot_mean(data, attribute, name, window):
    mean = getattr(data, attribute).interpolate().rolling(window).mean()
    std = getattr(data, attribute).interpolate().rolling(window).std()

    p.fill_between(mean.index,
                   mean + std,
                   mean - std,
                   alpha=0.5, color='C2', label='±std {}'.format(name)
                   )
    p.plot(data.index, getattr(data, attribute), c='C0', alpha=0.2,
           label='raw {}'.format(name))
    p.plot(mean.index, mean, c='C1',
           label='mean {}'.format(name))
    f = filtered_serie(getattr(data, attribute), 5e-6)
    f.name = name
    f.plot()

def _plot_boxplot_by_weekday(data, attribute, function='sum'):
    by_day = getattr(data.groupby(data.index.date), attribute)

    results_by_day = getattr(by_day, function)()
    results_by_day = pd.DataFrame(results_by_day, index=pd.to_datetime(results_by_day.index))
    results_by_day['weekday'] = results_by_day.apply(lambda x:x.index.weekday)
    results_by_day.boxplot(by='weekday')

def plot_mean_heart_rate(data, window=60*24):
    _plot_mean(data, 'HEART_RATE', 'HR', window)
    p.legend()

def plot_mean_raw_intensity(data, window=60*24):
    _plot_mean(data, 'RAW_INTENSITY', 'intensity', window)
    p.legend()

def plot_steps_boxplot_by_weekday(data):
    _plot_boxplot_by_weekday(data, 'STEPS', 'sum')

def plot_heart_rate_boxplot_by_weekday(data):
    _plot_boxplot_by_weekday(data, 'HEART_RATE', 'mean')

def plot_activity_boxplot_by_weekday(data):
    _plot_boxplot_by_weekday(data, 'RAW_INTENSITY', 'mean')

def plot_mean_week(data):
    fig, axis = p.subplots(3, 3, sharex=True, sharey=True)

    for (day, data_day) in data.groupby(data.index.weekday):
        ax = axis[p.unravel_index(day, axis.shape)]

        data_day_by_time = data_day.groupby('TIME')

        steps = data_day_by_time.STEPS.mean()
        intensity = data_day_by_time.RAW_INTENSITY.mean().rolling(20).mean()
        hr = data_day_by_time.HEART_RATE.mean().rolling(20).mean()
        activity = data_day_by_time.ACTIVITY.mean().rolling(5).mean()

        ax.plot(activity, c='C1', alpha=0.2)
        ax.plot(steps, c='C0', alpha=0.9)
        ax.plot(intensity, c='C2', alpha=0.7)
        ax.plot(hr, c='C3')

    p.show()

def plot_cumstep_by_day(data, goal=8000):
    gd = data.groupby(data.index.date)
    ax = p.subplot(211)
    gd.RAW_INTENSITY.plot()
    p.subplot(212, sharex=ax)
    gd.STEPS.cumsum().plot()
    p.axhline(goal, c='c', ls='--')

def plot_calendar_of_steps(data, goal=8000):
    calendar = p.ones((52, 7)) * p.nan

    steps_by_day = data.groupby(data.index.date).STEPS.sum()

    min_week = 52
    max_week = 0
    for day, steps in steps_by_day.items():
        weeknumber = day.isocalendar()[1]
        weekday = day.isoweekday() - 1

        min_week = min(weeknumber, min_week)
        max_week = max(weeknumber, max_week)
        calendar[weeknumber, weekday] = steps

    p.title('Number of steps per day')
    vmax = max(2*goal, p.nanmax(calendar))
    vmin = p.nanmin(calendar)
    p.imshow(calendar[min_week:max_week+1, :], vmin=vmin, vmax=vmax,
            aspect='auto',
            extent=(0, 6, max_week, min_week))
    p.colorbar(orientation='horizontal')

def plot_calendar_of_activity(data, goal=8000):
    calendar = p.ones((52, 7)) * p.nan

    activity_by_day = data.groupby(data.index.date).RAW_INTENSITY.sum()

    min_week = 52
    max_week = 0
    for day, activity in activity_by_day.items():
        weeknumber = day.isocalendar()[1]
        weekday = day.isoweekday() - 1

        min_week = min(weeknumber, min_week)
        max_week = max(weeknumber, max_week)
        calendar[weeknumber, weekday] = activity

    p.title('Number of activity per day')
    vmax = max(2*goal, p.nanmax(calendar))
    vmin = p.nanmin(calendar)
    p.imshow(calendar[min_week:max_week+1, :], vmin=vmin, vmax=vmax,
            aspect='auto', extent=(0, 6, max_week, min_week))
    p.colorbar(orientation='horizontal')

def plot_calendar_of_sleep_duration(data, goal=7.5):
    calendar = p.nan * p.ones((52, 7))
    min_week = 52
    max_week = 0

    for sleep in get_shifts(data, ActivityType.SLEEPING):
        fall_asleep_datetime = sleep.iloc[0].DATETIME
        wake_up_datetime = sleep.iloc[-1].DATETIME
        duration = (wake_up_datetime - fall_asleep_datetime).total_seconds()/(60*60)

        # Substract 12h, to be sure we got the good date. Eg:
        # Fall asleep | Fall asleep - 12h | date to be used
        # 04/23 23:20 |    04/23 11:30    |      04/23
        # 04/25 03:20 |    04/24 15:20    |      04/24
        # 04/25 22:00 |    04/25 22:20    |      04/25

        fall_asleep_datetime -= timedelta(hours=12)
        weeknumber = fall_asleep_datetime.isocalendar()[1]
        weekday = fall_asleep_datetime.isoweekday() - 1

        min_week = min(weeknumber, min_week)
        max_week = max(weeknumber, max_week)
        if p.isnan(calendar[weeknumber, weekday]):
            calendar[weeknumber, weekday] = duration
        else:
            calendar[weeknumber, weekday] += duration

    p.title('sleep duration')
    vmax = min(p.nanmax(calendar), 12)
    vmin = p.nanmin(calendar)
    p.imshow(calendar[min_week:max_week+1, :], vmin=vmin, vmax=vmax,
            aspect='auto', extent=(0, 6, max_week, min_week))
    p.colorbar(orientation='horizontal')

def plot_calendar_of_deepsleep_percentage(data, goal=7.5):
    calendar = p.nan * p.ones((52, 7))
    min_week = 52
    max_week = 0

    for date, durations in get_sleep_df(data).iterrows():
        weeknumber = date.isocalendar()[1]
        weekday = date.isoweekday() - 1

        total = durations.lightsleep + durations.deepsleep + durations.rem
        deepsleep_percent = durations.rem / total * 100

        calendar[weeknumber, weekday] = deepsleep_percent

        min_week = min(weeknumber, min_week)
        max_week = max(weeknumber, max_week)

    p.title('deepsleep duration (%)')
    vmax = None
    vmin = None
    p.imshow(calendar[min_week:max_week+1, :], vmin=vmin, vmax=vmax,
             aspect='auto', extent=(0, 6, max_week, min_week))
    p.colorbar(orientation='horizontal')

def plot_calendar_of_sleep_score(data, goal=7.5):
    calendar = p.nan * p.ones((52, 7))
    min_week = 52
    max_week = 0

    for date, sc in get_sleep_score(get_sleep_df(data), goal).iterrows():
        weeknumber = date.isocalendar()[1]
        weekday = date.isoweekday() - 1
        calendar[weeknumber, weekday] = sc.total

        min_week = min(weeknumber, min_week)
        max_week = max(weeknumber, max_week)

    p.title('sleep score')
    vmax = 100
    vmin = min(60, p.nanmin(calendar))
    p.imshow(calendar[min_week:max_week+1, :], vmin=vmin, vmax=vmax,
             cmap='cool', aspect='auto', extent=(0, 6, max_week, min_week))
    p.colorbar(orientation='horizontal')

def plot_sleep_score(data, goal=7.5):
    fig, ax1 = p.subplots()

    sleep_df = get_sleep_df(data)
    sc = get_sleep_score(sleep_df, goal)

    ax1.bar(sleep_df.index, sleep_df.deepsleep + sleep_df.rem + sleep_df.lightsleep,
            label='total sleep')
    ax1.bar(sleep_df.index, sleep_df.deepsleep + sleep_df.rem,
            label='deepsleep + rem')
    ax1.bar(sleep_df.index, sleep_df.rem, label='rem')


    if len(sleep_df) < 10: # if less than 10 days, then print durations
        y_offset = -0.5
        for patch in ax1.patches:
            b = patch.get_bbox()
            duration = p.ceil((b.y1 - b.y0)*60)
            minutes, hours = int(duration % 60), int(duration // 60)
            val = "{:d}h {:02d}min".format(hours, minutes)
            ax1.annotate(val, ((b.x0 + b.x1)/2, b.y1 + y_offset),
                         ha='center')

    ax1.axhline(goal, c='m', ls='--', alpha=0.3)
    ax1.set_ylabel('duration (h)')
    ax1.legend(loc='upper left')

    ax2 = ax1.twinx()

    k = ['lightsleep', 'deepsleep', 'rem']
    normalized_durations = sleep_df[k]
    normalized_durations = normalized_durations.div(normalized_durations.sum(1), 0)*100

    ax2.plot(sleep_df.index, sc.total, marker='o', color='C4',
             label='sleep score')
    ax2.plot(normalized_durations.index,
             normalized_durations.deepsleep + normalized_durations.rem,
             marker='o', color='C2', label='% (deepsleep + rem)')
    ax2.plot(normalized_durations.index,
             normalized_durations.rem,
             marker='o', color='C3', label='% rem')
    ax2.set_ylim(0, 100)
    ax2.legend(loc='upper right')
    p.show()


if __name__ == '__main__':
    data = loaddata('miband.db', cached=True)
    functions = (
                 plot_mean_heart_rate,
                 plot_mean_raw_intensity,
                 plot_cumstep_by_day,
                 plot_steps_boxplot_by_weekday,
                 plot_heart_rate_boxplot_by_weekday,
                 plot_activity_boxplot_by_weekday,
                 plot_mean_week,
                 plot_calendar_of_steps,
                 plot_calendar_of_activity,
                 plot_calendar_of_sleep_duration,
                 #plot_calendar_of_deepsleep_percentage,
                 ##plot_calendar_of_sleep_score,
                 #plot_sleep_score,
                )
    for f in functions:
        f(data)
        p.show()
