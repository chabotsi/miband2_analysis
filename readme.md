Analysis data from your miband2, using the gadgetbrigde database.

It assumes that your database is name "miband.db".


# Examples

    python3 ./visualisation.py
    python3 ./sleep.py
    
```
from model import loaddata
data = loaddata('miband.db', '2 weeks ago')
data.ACTIVITY.plot()
```